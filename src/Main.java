import sky.algorithm.BasicSort;

public class Main {

    public static void main(String[] args) {
        int[] array = {1, 3, 2, 10, 4};
        BasicSort sort = new BasicSort();
        sort.mergeSort(array);
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + ", ");
        }
        System.out.println("Hello World!");
    }
}
