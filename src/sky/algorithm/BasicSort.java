package sky.algorithm;

public class BasicSort {
    /**
     * 插入排序
     * 时间复杂度O(n2)
     * 空间复杂度O(1): 仅需要i、j、current三个变量空间
     * @param array 输入待排序数组
     */
    public void insertionSort(int[] array) {
        int i,j,current;
        for (i = 1; i < array.length; i++) {
            //缓存当前索引和值
            j = i;
            current = array[j];
            //进行插入比较
            while(j > 0 && current < array[j - 1]) {
                array[j] = array[j - 1];
                j--;
            }
            array[j] = current;
        }
    }

    /**
     * 选择排序
     * 时间复杂度O(n2)
     * 空间复杂度O(1): 仅需要index、minimum三个变量空间
     * @param array 输入待排序数组
     */
    public void selectionSort(int[] array) {
        int index,minimum;
        for (int i = 0; i < array.length; i++) {
            index = i;
            minimum = array[i];
            for(int j = i + 1; j < array.length; j++) {
                if(array[j] < array[index]) {
                    index = j;
                }
            }
            array[i] = array[index];
            array[index] = minimum;
        }
    }

    private int[] mergeTempArray;
    public void mergeSort(int[] array) {
        mergeTempArray = new int[array.length];
        mergeSort(array,0, array.length - 1);
    }
    /**
     * 归并排序
     *
     */
    private void mergeSort(int[] array, int start, int end) {
        if(start < end) {
            int mid = (start + end) / 2;
            mergeSort(array, start, mid);
            mergeSort(array, mid + 1, end);
            mergeArray(array, start, mid, end);
        }
    }

    private void mergeArray(int[] array,int start, int middle, int end) {
        System.out.printf("start %d middle %d end %d",start,middle,end);
        int p = start, q = middle + 1;
        int k = p;
        while (p <= middle && q <= end) {
            if(array[p] >= array[q]) {
                mergeTempArray[k] = array[q];
                q++;
            } else {
                mergeTempArray[k] = array[p];
                p++;
            }
            k++;
        }
        while (p <= middle) {
            mergeTempArray[k] = array[p];
            k++;
            p++;
        }

        while (q <= end) {
            mergeTempArray[k] = array[q];
            k++;
            q++;
        }

        for(int i = start; i <= end; i++) {
            array[i] = mergeTempArray[i];
        }

    }

    /**
     * worst is O(n2) but average-case running time is as good as O(nlgn)
     * the constant factor hidden in the big-O is quite good.
     * @param array
     */
    private void quickSort(int[] array) {
        int p = array.length -1;

    }

    private void quickSort(int[] array, int start, int end) {
        int p = end;
        int current = array[p];

    }


}
