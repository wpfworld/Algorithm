package sky.algorithm.uf;

public class QuickUnionUF {
    int[] numbers;

    public QuickUnionUF(int N) {
        this.numbers = new int[N];
        for(int i = 0; i < numbers.length; i++) {
            numbers[i] = i;
        }
    }

    public boolean connected(int p, int q) {
        return root(p) == root(q);
    }

    public void union(int p, int q) {
        int r1 = root(p);
        int r2 = root(q);
        numbers[r1] = r2;
    }

    private int root(int p) {
        while(p != numbers[p]){
            p = numbers[p];
        }
        return p;
    }

    public String output(int p) {
        return root(p) + "";
    }

}
