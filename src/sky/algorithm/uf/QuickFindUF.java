package sky.algorithm.uf;

public class QuickFindUF {
    int[] numbers;
    public QuickFindUF(int N) {
       numbers = new int[N];
       for(int i = 0; i < N; i++){
           numbers[i] = i;
       }
    }

    public boolean connected(int p, int q){
        if(numbers[p] == numbers[q]){
            return true;
        } else return false;
    }

    public void union(int p, int q) {
        int pVal = numbers[p];
        int qVal = numbers[q];
        for(int i=0; i < numbers.length; i++) {
            if(numbers[i] == pVal) {
                numbers[i] = qVal;
            }
        }
    }

    public int output(int p){
        return numbers[p];
    }

}
