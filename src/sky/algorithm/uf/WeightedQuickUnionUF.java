package sky.algorithm.uf;

public class WeightedQuickUnionUF {
    int[] ids;
    int[] weight;

    public WeightedQuickUnionUF(int N){
        ids = new int[N];
        weight = new int[N];
        for(int i = 0; i < N; i++) {
            ids[i] = i;
        }
        for(int i = 0; i < N; i++){
            weight[i] = 1;
        }
    }

    public boolean connected(int p, int q) {
        return root(p) == root(q);
    }

    public void union(int p, int q) {
        int i = root(p);
        int j = root(q);
        if(weight[i] > weight[j]) {
            weight[i] += weight[j];
            ids[j] = i;
        } else {
            weight[j] +=weight[i];
            ids[j] = i;
        }
    }

    public String output(int p) {
        return root(p) + "";
    }


    private int root(int p) {
        while(p != ids[p]) {
            p = ids[p];
        }
        return p;
    }

}
