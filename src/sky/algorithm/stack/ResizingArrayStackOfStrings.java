package sky.algorithm.stack;

public class ResizingArrayStackOfStrings {

    String[] array;
    int index = 0;

    public ResizingArrayStackOfStrings() {
        array = new String[2];
    }

    public boolean isEmpty() {
        return index == 0;
    }

    public void push(String str) {
        if(index == array.length) {
            resize(2 * array.length);
        }
        array[index] = str;
        index ++;
    }

    public String pop() {
        if(isEmpty()) {
            return null;
        }
        index--;
        String str = array[index];
        array[index] = null;
        if(index > 0 && index == array.length / 4) {
            resize(array.length / 2);
        }
        return str;
    }

    private void resize(int length) {
        String[] array = new String[length];
        for(int i = 0; i < index; i++) {
            array[i] = this.array[i];
        }
        this.array = array;
    }

}
