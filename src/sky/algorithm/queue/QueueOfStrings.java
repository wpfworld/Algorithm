package sky.algorithm.queue;

public class QueueOfStrings {

    private class Node {
        String value;
        Node next;
    }

    private Node first,last;

    public void enqueue(String obj) {
        Node oldLast = last;
        last = new Node();
        last.value = obj;
        last.next = null;
        if(isEmpty()) first = last;
        else {
            oldLast.next = last;
        }
    }

    public String dequeue() {
        if(isEmpty()) return null;
        String item = first.value;
        first = first.next;
        if(isEmpty()) last = null;
        return item;
    }

    public boolean isEmpty(){
        return first == null;
    }
}
