package sky.algorithm;

import java.util.ArrayList;

public class BasicSearch {

    public <T extends Comparable> int binarySearch(T[] array, T target) {
        int min = 0;
        int max = array.length - 1;
        int guess;
        while (min <= max) {
            guess = (min + max) / 2;
            T item = array[guess];
            if(item.compareTo(target) == 0) {
                return guess;
            } else if (item.compareTo(target) < 0) {
                min = guess + 1;
            } else {
                max = guess - 1;
            }
        }
        return -1;
    }

}
