package sky.algorithm.stack;

import org.junit.Assert;
import org.junit.Test;

public class ResizingArrayStackOfStringsTest {
    ResizingArrayStackOfStrings stack = new ResizingArrayStackOfStrings();

    @Test
    public void testPush() {
        stack.push("Hello");
        stack.push(" ");
        stack.push("world");
        stack.push("!");
    }

    @Test
    public void testPop() {
        testPush();
        String actualValue = stack.pop();
        Assert.assertEquals("!", actualValue);
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
        Assert.assertTrue(stack.isEmpty());

    }

}
