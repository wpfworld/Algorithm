package sky.algorithm.queue;

import org.junit.Assert;
import org.junit.Test;

public class QueueOfStringsTest {

    @Test
    public void testEnqueue() {
        QueueOfStrings queueOfStrings = new QueueOfStrings();
         queueOfStrings.enqueue("my");
        Assert.assertFalse(queueOfStrings.isEmpty());
    }

    @Test
    public void testQueue() {
        QueueOfStrings queueOfStrings = new QueueOfStrings();
        String actualValue = queueOfStrings.dequeue();
        Assert.assertEquals(null, actualValue);

        queueOfStrings.enqueue("wpf");
        actualValue = queueOfStrings.dequeue();
        Assert.assertEquals("wpf", actualValue);

        //FIFO
        queueOfStrings.enqueue("Hello");
        queueOfStrings.enqueue("world");

        actualValue = queueOfStrings.dequeue();
        Assert.assertEquals("Hello", actualValue);
    }


}
