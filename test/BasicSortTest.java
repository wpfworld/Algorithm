import org.junit.Assert;
import org.junit.Test;
import sky.algorithm.BasicSort;

public class BasicSortTest {

    @Test
    public void testInsertionSort()
    {
        int[] array = {1, 3, 2, 10, 4};
        BasicSort basicSort = new BasicSort();
        basicSort.insertionSort(array);
        Assert.assertEquals("1,2,3,4,10", arrayJoin(array));
    }

    @Test
    public void testSelectionSort() {
        int[] array = {1, 3, 2, 10, 4};
        BasicSort basicSort = new BasicSort();
        basicSort.selectionSort(array);
        Assert.assertEquals("1,2,3,4,10", arrayJoin(array));
    }

    @Test
    public void testMergeSort() {
        int[] array = {1, 3, 2, 10, 4};
        BasicSort basicSort = new BasicSort();
        basicSort.mergeSort(array);
        Assert.assertEquals("1,2,3,4,10", arrayJoin(array));
    }

    private String arrayJoin(int[] array)
    {
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < array.length; i++) {
            stringBuilder.append(array[i]);
            stringBuilder.append(",");
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        return stringBuilder.toString();
    }

}
