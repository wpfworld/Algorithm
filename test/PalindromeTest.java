import org.junit.Assert;
import org.junit.Test;
import sky.algorithm.Palindrome;

public class PalindromeTest {
    @Test
    public void testIsPalindrome()
    {
        Assert.assertEquals(true, Palindrome.isPalindrome("a"));
        Assert.assertEquals(true, Palindrome.isPalindrome(""));

        Assert.assertEquals(false, Palindrome.isPalindrome("happy"));
        Assert.assertEquals(true, Palindrome.isPalindrome("abbabba"));
    }

}
