import org.junit.Assert;
import org.junit.Test;
import sky.algorithm.BasicSearch;

public class BasicSearchTest {
    @Test
    public void testBinarySearch() {
        Integer[] array = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        Integer find = 0;
        BasicSearch basicSearch = new BasicSearch();
        int index = basicSearch.binarySearch(array, find);
        Assert.assertEquals(0, index);

        find = 6;
        index = basicSearch.binarySearch(array, find);
        Assert.assertEquals(6, index);

        find = 9;
        index = basicSearch.binarySearch(array, find);
        Assert.assertEquals(9, index);
    }
}
