package uf;

import org.junit.Assert;
import org.junit.Test;
import sky.algorithm.uf.QuickFindUF;

public class QuickFindUFTest {
    @Test
    public void testUnion(){
        QuickFindUF uf = new QuickFindUF(10);
        uf.union(0,1);
        uf.union(1,5);
        uf.union(5,6);
        uf.union(6,8);

        uf.union(2,3);

        uf.union(4,7);
        uf.union(7,9);

        Assert.assertTrue(uf.connected(0,8));
        Assert.assertTrue(uf.connected(4,9));
        Assert.assertTrue(!uf.connected(2,0));
        System.out.println(uf.output(8));
    }
}
